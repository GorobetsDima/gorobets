<%@include file="/WEB-INF/include/main.jsp" %>
<html>
<head>
    <title>Show Votings</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.js"></script>
    <script src="resources/js/voting.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css">
    <link rel="stylesheet" href="resources/css/voting.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="control-group" id="votings">

                <label class="control-label" for="votings">All Votings!</label>
                <label class="control-label" for="votings">You can take part in these votings! </label>

                <div class="controls">

                    <%--<c:forEach var="voting" items="${votings}">--%>
                        <form action="/vote" method="post">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-offset-1 col-sm-8">
                                        <input class="btn btn-primary add-user" type="button" value=${voting.name}1111>
                                    </div>
                                </div>

                            </div>
                            <%--<div class="row">--%>
                                <%--<div class="show-voting">--%>
                                    <%--<div class="col-sm-offset-1 col-sm-8">--%>
                                        <%--<a class="voting" href="http://localhost:8080/vote">${voting.name}1</a>--%>
                                    <%--</div>--%>
                                <%--</div>--%>
                            <%--</div>--%>
                        </form>
                    <%--</c:forEach>--%>


                </div>

        </div>
    </div>
</div>
</body>
</html>
