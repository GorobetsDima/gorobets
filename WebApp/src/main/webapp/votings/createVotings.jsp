<%@include file="/WEB-INF/include/main.jsp" %>
<html>
<head>
    <title>Create Voting</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.js"></script>
    <script src="resources/js/voting.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css">
    <link rel="stylesheet" href="resources/css/voting.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="create-form">
            <div class="col-sm-offset-1 col-sm-8">
                <a class="create-voting" href="http://localhost:8080/createVoting">Create a voting</a>

            </div>
        </div>

    </div>
</div>
<div class="container">
    <div class="row">
        <div class="show-form">
            <div class="col-sm-offset-1 col-sm-8">
                <a class="show-votings" href="http://localhost:8080/showVotings">Show all votings</a>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="control-group" id="fields">

                <label class="control-label" for="fields">Create your own voting!</label>

                <div class="controls">

                    <form role="form" autocomplete="off">
                        <div class="input-group col-xs-3">
                            <input class="form-control" name="votingName" type="text" placeholder="Type voting name"
                                   value=""/>
                        </div>
                    </form>
                    <br>

                    <form role="form" autocomplete="off">
                        <div class="input-group col-xs-3">
                            <input class="form-control" name="option1" type="text" placeholder="Type option"
                                   value=""/>
                        </div>
                    </form>
                    <br>

                    <form role="form" autocomplete="off">
                        <div class="input-group col-xs-3">
                            <input class="form-control" name="option2" type="text" placeholder="Type option"
                                   value=""/>
                        </div>
                    </form>
                    <br>

                    <form role="form" autocomplete="off">
                        <div class="entry input-group col-xs-3">
                            <input class="form-control" name="fields[]" type="text" placeholder="Type option"
                                   value=""/>
                    	<span class="input-group-btn">
                            <button class="btn btn-success btn-add" type="button">
                                <span class="glyphicon glyphicon-plus"></span>
                            </button>
                        </span>
                        </div>
                        <br>
                    </form>
                    <small>Press <span class="glyphicon glyphicon-plus gs"></span> to add another form field</small>
                </div>

                <div class="row">
                    <div class="create-voting">
                        <div class="col-sm-offset-1 col-sm-8">
                            <%--<a class="voting" href="http://localhost:8080/vote">Create</a>--%>
                            <input class="btn btn-primary create-voting" type="button" value="Create">
                        </div>
                    </div>
                </div>

        </div>
        <br>

        <div class="container">
            <div class="row">
                <div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-default active">Anonymous voting
                        <input type="radio" name="options" id="option2" autocomplete="off" chacked>
                        <span class="glyphicon glyphicon-ok"></span>
                    </label>

                    <label class="btn btn-default">Public voting
                        <input type="radio" name="options" id="option1" autocomplete="off">
                        <span class="glyphicon glyphicon-ok"></span>
                    </label>


                </div>
            </div>

        </div>
        <%--<label class="radio-inline"><input type="radio" name="optradio">Anonymous voting</label>--%>
        <%--<label class="radio-inline"><input type="radio" name="optradio">Public voting</label>--%>


    </div>
</div>
</body>
</html>
