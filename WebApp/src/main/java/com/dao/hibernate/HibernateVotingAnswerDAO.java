package com.dao.hibernate;

import com.dao.DAO;
import com.dao.exception.DAOException;
import com.entity.VotingOption;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.util.List;

/**
 *
 */
public class HibernateVotingAnswerDAO implements DAO<VotingOption> {

    private static final Logger LOGGER = LogManager.getLogger();

    @Override
    public VotingOption create(VotingOption votingOption) throws DAOException {

        VotingOption persistedVotingOption;
        Session session = null;
        try {
            HibernateUtil.getSessionFactory().openSession();

            session = HibernateUtil.getSessionFactory().getCurrentSession();

            session.beginTransaction();
            session.save(votingOption);
            int id = votingOption.getId();
            persistedVotingOption = (VotingOption) session.load(VotingOption.class, id);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            LOGGER.error("HibernateException during votingOption create method", he);
            throw new DAOException(he);
        } finally {
            if (session != null && session.isOpen()) {
                try {
                    session.close();
                } catch (HibernateException he) {
                    LOGGER.error("Can't close Session for votingOption create method" + session + " Cause: ", he);
                    throw new DAOException(he);
                }
            }
        }


        return persistedVotingOption;
    }

    /**
     * Get method - get VotingOption from the DB storage by VotingOption id.
     *
     * @param id - id of VotingOption instance with int type
     * @return VotingOption instance from DB
     * @throws DAOException
     */
    @Override
    public VotingOption get(int id) throws DAOException {
        VotingOption persistedVotingOption;
        Session session = null;
        try {
            HibernateUtil.getSessionFactory().openSession();
            session = HibernateUtil.getSessionFactory().getCurrentSession();


            persistedVotingOption = (VotingOption) session.load(VotingOption.class, id);

        } catch (HibernateException he) {
            LOGGER.error("HibernateException during VotingOption get method", he);
            throw new DAOException(he);
        } finally {
            if (session != null&& session.isOpen()) {
                try {
                    session.close();
                } catch (HibernateException he) {
                    LOGGER.error("Can't close Session for VotingOption get method" + session + " Cause: ", he);
                    throw new DAOException(he);
                }
            }
        }
        return persistedVotingOption;
    }

    /**
     * Update method - update Voting at the DB storage
     *
     * @param votingOption - Voting instance
     * @return Voting instance from DB
     * @throws DAOException
     */
    @Override
    public VotingOption update(VotingOption votingOption) throws DAOException {
        VotingOption persistedVotingOption;
        Session session = null;
        try {
            HibernateUtil.getSessionFactory().openSession();
            session = HibernateUtil.getSessionFactory().getCurrentSession();

            session.beginTransaction();

            session.update(votingOption);
            persistedVotingOption = (VotingOption) session.load(VotingOption.class, votingOption.getId());

            session.getTransaction().commit();
        } catch (HibernateException he) {
            LOGGER.error("HibernateException during votingOption update method", he);
            throw new DAOException(he);
        } finally {
            if (session != null&& session.isOpen()) {
                try {
                    session.close();
                } catch (HibernateException he) {
                    LOGGER.error("Can't close Session for votingOption update method" + session + " Cause: ", he);
                    throw new DAOException(he);
                }
            }
        }
        return persistedVotingOption;
    }

    /**
     * Remove method - remove Voting from the DB storage by Voting id
     *
     * @param id - id of Voting instance with int type
     * @throws DAOException
     */
    @Override
    public void remove(int id) throws DAOException {

        Session session = null;
        try {
            HibernateUtil.getSessionFactory().openSession();

            session = HibernateUtil.getSessionFactory().getCurrentSession();

            VotingOption persistedVotingOption = (VotingOption) session.load(VotingOption.class, id);

            session.beginTransaction();
            session.delete(persistedVotingOption);
            session.getTransaction().commit();

        } catch (HibernateException he) {
            LOGGER.error("HibernateException during VotingOption remove method", he);
            throw new DAOException(he);
        } finally {
            if (session != null&& session.isOpen()) {
                try {
                    session.close();
                } catch (HibernateException he) {
                    LOGGER.error("Can't close Session for VotingOption remove method" + session + " Cause: ", he);
                    throw new DAOException(he);
                }
            }
        }

    }

    /**
     * GetAll method - get List of VotingOption from the DB storage
     *
     * @return List<T> -list with VotingAnswers
     * @throws DAOException
     */
    @Override
    public List<VotingOption> getAll() throws DAOException {

        List<VotingOption> votingOptionList;
        Session session = null;
        try {
            HibernateUtil.getSessionFactory().openSession();
            session = HibernateUtil.getSessionFactory().getCurrentSession();

            session.beginTransaction();
            votingOptionList = (List<VotingOption>) session.createCriteria(VotingOption.class).list();
            session.getTransaction().commit();

        } catch (HibernateException he) {
            LOGGER.error("HibernateException during VotingOption getAll method", he);
            throw new DAOException(he);
        } finally {
            if (session != null && session.isOpen()) {
                try {
                    session.close();
                } catch (HibernateException he) {
                    LOGGER.error("Can't close Session for VotingOption getAll method" + session + " Cause: ", he);
                    throw new DAOException(he);
                }
            }
        }

        return votingOptionList;
    }

}
