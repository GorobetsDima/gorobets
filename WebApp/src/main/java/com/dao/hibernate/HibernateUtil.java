package com.dao.hibernate;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {
	
	private static final SessionFactory sessionFactory = buildSessionFactory();

	private static SessionFactory buildSessionFactory() throws HibernateException{
		// Create the SessionFactory from hibernate.cfg.xml

		Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties()).build();
		return configuration.setNamingStrategy(CustomNamingStrategy.INSTANCE).buildSessionFactory(serviceRegistry);
	}

	public static SessionFactory getSessionFactory() throws HibernateException{
		return sessionFactory;
	}

}
