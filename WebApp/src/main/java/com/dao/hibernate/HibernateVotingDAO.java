package com.dao.hibernate;

import com.dao.DAO;
import com.dao.exception.DAOException;
import com.entity.Voting;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.util.List;

/**
 *
 */
public class HibernateVotingDAO implements DAO<Voting> {
    private static final Logger LOGGER = LogManager.getLogger();

    @Override
    public Voting create(Voting voting) throws DAOException {

        Voting persistedVoting;
        Session session = null;
        try {
//            HibernateUtil.getSessionFactory().openSession();

            session = HibernateUtil.getSessionFactory().getCurrentSession();

            session.beginTransaction();
            session.save(voting);
            int id = voting.getId();
            persistedVoting = (Voting) session.load(Voting.class, id);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            LOGGER.error("HibernateException during voting create method", he);
            throw new DAOException(he);
        } finally {
            if (session != null && session.isOpen()) {
                try {
//                    session.close();
                } catch (HibernateException he) {
                    LOGGER.error("Can't close Session for voting create method" + session + " Cause: ", he);
                    throw new DAOException(he);
                }
            }
        }


        return persistedVoting;
    }

    /**
     * Get method - get Voting from the DB storage by Voting id.
     *
     * @param id - id of Voting instance with int type
     * @return Voting instance from DB
     * @throws DAOException
     */
    @Override
    public Voting get(int id) throws DAOException {
        Voting persistedVoting;
        Session session = null;
        try {
            HibernateUtil.getSessionFactory().openSession();
            session = HibernateUtil.getSessionFactory().getCurrentSession();


            persistedVoting = (Voting) session.load(Voting.class, id);

        } catch (HibernateException he) {
            LOGGER.error("HibernateException during voting get method", he);
            throw new DAOException(he);
        } finally {
            if (session != null&& session.isOpen()) {
                try {
                    session.close();
                } catch (HibernateException he) {
                    LOGGER.error("Can't close Session for voting get method" + session + " Cause: ", he);
                    throw new DAOException(he);
                }
            }
        }
        return persistedVoting;
    }

    /**
     * Update method - update Voting at the DB storage
     *
     * @param voting - Voting instance
     * @return Voting instance from DB
     * @throws DAOException
     */
    @Override
    public Voting update(Voting voting) throws DAOException {
        Voting persistedVoting;
        Session session = null;
        try {
            HibernateUtil.getSessionFactory().openSession();
            session = HibernateUtil.getSessionFactory().getCurrentSession();

            session.beginTransaction();

            session.update(voting);
            persistedVoting = (Voting) session.load(Voting.class, voting.getId());

            session.getTransaction().commit();
        } catch (HibernateException he) {
            LOGGER.error("HibernateException during voting update method", he);
            throw new DAOException(he);
        } finally {
            if (session != null&& session.isOpen()) {
                try {
                    session.close();
                } catch (HibernateException he) {
                    LOGGER.error("Can't close Session for voting update method" + session + " Cause: ", he);
                    throw new DAOException(he);
                }
            }
        }
        return persistedVoting;
    }

    /**
     * Remove method - remove Voting from the DB storage by Voting id
     *
     * @param id - id of Voting instance with int type
     * @throws DAOException
     */
    @Override
    public void remove(int id) throws DAOException {

        Session session = null;
        try {
            HibernateUtil.getSessionFactory().openSession();

            session = HibernateUtil.getSessionFactory().getCurrentSession();

            Voting persistedVoting = (Voting) session.load(Voting.class, id);

            session.beginTransaction();
            session.delete(persistedVoting);
            session.getTransaction().commit();

        } catch (HibernateException he) {
            LOGGER.error("HibernateException during voting remove method", he);
            throw new DAOException(he);
        } finally {
            if (session != null&& session.isOpen()) {
                try {
                    session.close();
                } catch (HibernateException he) {
                    LOGGER.error("Can't close Session for voting remove method" + session + " Cause: ", he);
                    throw new DAOException(he);
                }
            }
        }

    }

    /**
     * GetAll method - get List of Voting from the DB storage
     *
     * @return List<T> -list with Votings
     * @throws DAOException
     */
    @Override
    public List<Voting> getAll() throws DAOException {

        List<Voting> votingsList;
        Session session = null;
        try {
            HibernateUtil.getSessionFactory().openSession();
            session = HibernateUtil.getSessionFactory().getCurrentSession();

            session.beginTransaction();
            votingsList = (List<Voting>) session.createCriteria(Voting.class).list();
            session.getTransaction().commit();

        } catch (HibernateException he) {
            LOGGER.error("HibernateException during voting getAll method", he);
            throw new DAOException(he);
        } finally {
            if (session != null && session.isOpen()) {
                try {
                    session.close();
                } catch (HibernateException he) {
                    LOGGER.error("Can't close Session for voting getAll method" + session + " Cause: ", he);
                    throw new DAOException(he);
                }
            }
        }

        return votingsList;
    }

}
