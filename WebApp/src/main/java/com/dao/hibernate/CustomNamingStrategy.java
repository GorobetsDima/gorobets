package com.dao.hibernate;

import org.hibernate.cfg.ImprovedNamingStrategy;

/**
 * A custom naming strategy implementation which uses following naming conventions:
 * <p>
 * Table names are lower case and in plural form. Words are separated with '_' character.
 * Column names are lower case and words are separated with '_' character.
 */
public class CustomNamingStrategy extends ImprovedNamingStrategy {

    private static final String GOR_PREFIX = "gor";

    /**
     * Transforms class names to table names by using the described naming conventions.
     *
     * @param className
     * @return The constructed table name.
     */
    @Override
    public String classToTableName(String className) {
        String tableName = super.classToTableName(className);
        return transformToCustomForm(tableName);
    }

    private String transformToCustomForm(String tableName) {
        StringBuilder customForm = new StringBuilder();

        customForm.append(GOR_PREFIX);
        customForm.append(tableName);


        return customForm.toString();
    }
}
