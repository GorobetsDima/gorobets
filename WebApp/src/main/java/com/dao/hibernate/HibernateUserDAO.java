package com.dao.hibernate;

import com.dao.UserDAO;
import com.dao.exception.DAOException;
import com.entity.user.User;
import com.entity.Voting;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.util.List;

/**
 *
 */
public class HibernateUserDAO implements UserDAO {

    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * *
     * Create method - adds User to the DB
     *
     * @param user - User instance
     * @return User instance from DB
     * @throws DAOException
     */
    @Override
    public User create(User user) throws DAOException {

        User persistedUser;
        Session session = null;
        try {
//            HibernateUtil.getSessionFactory().openSession();

            session = HibernateUtil.getSessionFactory().getCurrentSession();

            session.beginTransaction();
            session.save(user);
            int id = user.getId();
            persistedUser = (User) session.load(User.class, id);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            LOGGER.error("HibernateException during user create method", he);
            throw new DAOException(he);
        } finally {
            if (session != null && session.isOpen()) {
                try {
//                  session.close();
                } catch (HibernateException he) {
                    LOGGER.error("Can't close Session for user create method" + session + " Cause: ", he);
                    throw new DAOException(he);
                }
            }
        }


        return persistedUser;
    }

    /**
     * Get method - get User from the DB storage by User id.
     *
     * @param id - id of User instance with int type
     * @return User instance from DB
     * @throws DAOException
     */
    @Override
    public User get(int id) throws DAOException {
        User persistedUser;
        Session session = null;
        try {
            HibernateUtil.getSessionFactory().openSession();
            session = HibernateUtil.getSessionFactory().getCurrentSession();


            persistedUser = (User) session.load(User.class, id);

        } catch (HibernateException he) {
            LOGGER.error("HibernateException during user get method", he);
            throw new DAOException(he);
        } finally {
            if (session != null&& session.isOpen()) {
                try {
                    session.close();
                } catch (HibernateException he) {
                    LOGGER.error("Can't close Session for user get method" + session + " Cause: ", he);
                    throw new DAOException(he);
                }
            }
        }
        return persistedUser;
    }

    /**
     * Update method - update User at the DB storage
     *
     * @param user - User instance
     * @return User instance from DB
     * @throws DAOException
     */
    @Override
    public User update(User user) throws DAOException {
        User persistedUser;
        Session session = null;
        try {
            HibernateUtil.getSessionFactory().openSession();
            session = HibernateUtil.getSessionFactory().getCurrentSession();

            session.beginTransaction();

            session.update(user);
            persistedUser = (User) session.load(User.class, user.getId());

            session.getTransaction().commit();
        } catch (HibernateException he) {
            LOGGER.error("HibernateException during user update method", he);
            throw new DAOException(he);
        } finally {
            if (session != null&& session.isOpen()) {
                try {
                    session.close();
                } catch (HibernateException he) {
                    LOGGER.error("Can't close Session for user update method" + session + " Cause: ", he);
                    throw new DAOException(he);
                }
            }
        }
        return persistedUser;
    }

    /**
     * Remove method - remove User from the DB storage by User id
     *
     * @param id - id of User instance with int type
     * @throws DAOException
     */
    @Override
    public void remove(int id) throws DAOException {

        Session session = null;
        try {
//            HibernateUtil.getSessionFactory().openSession();

            session = HibernateUtil.getSessionFactory().getCurrentSession();

            User persistedUser = (User) session.load(User.class, id);

            session.beginTransaction();
            session.delete(persistedUser);
            session.getTransaction().commit();

        } catch (HibernateException he) {
            LOGGER.error("HibernateException during user remove method", he);
            throw new DAOException(he);
        } finally {
            if (session != null&& session.isOpen()) {
                try {
//                    session.close();
                } catch (HibernateException he) {
                    LOGGER.error("Can't close Session for user remove method" + session + " Cause: ", he);
                    throw new DAOException(he);
                }
            }
        }

    }

    /**
     * GetAll method - get List of User from the DB storage
     *
     * @return List<T> -list with Users
     * @throws DAOException
     */
    @Override
    public List<User> getAll() throws DAOException {

        List<User> usersList;
        Session session = null;
        try {
//            HibernateUtil.getSessionFactory().openSession();
            session = HibernateUtil.getSessionFactory().getCurrentSession();

            session.beginTransaction();
            usersList = (List<User>) session.createCriteria(User.class).list();
            session.getTransaction().commit();

        } catch (HibernateException he) {
            LOGGER.error("HibernateException during user getAll method", he);
            throw new DAOException(he);
        } finally {
            if (session != null && session.isOpen()) {
                try {
//                    session.close();
                } catch (HibernateException he) {
                    LOGGER.error("Can't close Session for user getAll method" + session + " Cause: ", he);
                    throw new DAOException(he);
                }
            }
        }

        return usersList;
    }

    /**
     * *
     * This method  get a User by userSurName from the DB storage.
     *
     * @param userSurName -SurName of user
     * @return User instance from DB
     * @throws DAOException
     */
    @Override
    public User getUserByUserSurName(String userSurName) throws DAOException {
        return null;
    }

    /**
     * This method  get a User by email from the DB storage.
     *
     * @param email -email of user
     * @return User instance from DB
     * @throws DAOException
     */

    @Override
    public User getUserByUserEmail(String email) throws DAOException {
        User persistedUser;
        Session session = null;
        try {
            HibernateUtil.getSessionFactory().openSession();
            session = HibernateUtil.getSessionFactory().getCurrentSession();

            session.beginTransaction();

            persistedUser = (User) session.createQuery("from User where email=: email").setParameter("email", email);

            session.getTransaction().commit();

        } catch (HibernateException he) {
            LOGGER.error("HibernateException during user getUserByUserEmail method", he);
            throw new DAOException(he);
        } finally {
            if (session != null) {
                try {
                    HibernateUtil.getSessionFactory().close();
                } catch (HibernateException he) {
                    LOGGER.error("Can't close Session for user getUserByUserEmail method" + session + " Cause: ", he);
                    throw new DAOException(he);
                }
            }
        }
        return persistedUser;
    }

    @Override
    public List<Voting> getUserVotingsByUserId(int userId) throws DAOException {
        return null;
    }


}
