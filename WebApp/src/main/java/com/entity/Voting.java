package com.entity;

import com.entity.user.User;

import javax.persistence.*;
import java.util.Set;


/**
 * 1. На основе WEB-приложения регистрации реализовать функциональность
 * "Голосование". Пользователь может создавать свои голосования,
 * а также просматривать и участвовать в голосованиях созданных
 * другими пользователями.
 * 2. Пользователь может проголосовать в одном голосовании только один раз.
 * 3. Форма создания голосования должна содержать:
 * - поле для названия голосования (пример: "Ваше любимое животное")
 * - поля для ввода вариантов ответа (пример: Слон, Панда, Коала, Капибара,
 * Опоссум, Cвой вариант)
 * - реализовать возможность динамически увеличивать количество полей для
 * ввода вариантов ответа (например, изначально показывать 3 поля и кнопку
 * "добавить новый вариант", если количество полей недостаточно,
 * то каждое нажатие на кнопку добавляет новое).
 * 4. При создании голосования сделать опцию выбора между анонимным и не
 * анонимным голосованием.
 * 5. Результаты голосования отображать в следующем виде:
 * для анонимного голосования
 * Панда - 10
 * Коала - 12
 * Капибара - 0
 * Другое - 4
 * <p>
 * Для не анонимного голосования:
 * Панда - 3 (Sheldon, Penny, Amy)
 * Коала - 2 (Leonard, Hovard)
 * Капибара - 0
 * Другое - 1 (Raj)
 * <p>
 * 6. Все данные сохранять с помощью ORM Hibernate, использовать подход с
 * аннотациями.
 * 7. Тщательно продумайте архитектуру приложения и все зависимости.
 * В репозиторий залить UML-диаграмму классов и зависимостей приложения,
 * а так же скриншот схемы БД.
 */

/**
 * JavaBean class - Voting
 */
@Table(name = "votings")
@Entity
public class Voting {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(length = 500, unique = true, nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "User_id", nullable = false)
    private User author;

    @OneToMany
    private Set<VotingOption> options;

    public Voting() {
    }

    public Voting(String name, User author, Set<VotingOption> options) {
        this.name = name;
        this.author = author;
        this.options = options;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<VotingOption> getOptions() {
        return options;
    }

    public void setOptions(Set<VotingOption> options) {
        this.options = options;
    }


    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Voting{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", user=" + author +
                '}';
    }


}

