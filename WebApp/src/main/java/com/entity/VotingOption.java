package com.entity;

import com.entity.user.User;

import javax.persistence.*;
import java.util.Set;

/**
 *
 */
@Entity
@Table(name = "options")
public class VotingOption {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(length = 100)
    private String name;

    @ManyToOne
    private Voting voting;


//    @ManyToMany
//    private Set<User> users;

    public VotingOption() {
    }

    public VotingOption(String name) {
        this.name = name;

    }
//
//    public Set<User> getUsers() {
//        return users;
//    }
//
//    public void setUsers(Set<User> users) {
//        this.users = users;
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Voting getVoting() {
        return voting;
    }

    public void setVoting(Voting voting) {
        this.voting = voting;
    }

}
