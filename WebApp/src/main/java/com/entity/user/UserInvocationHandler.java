package com.entity.user;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 *
 */
public class UserInvocationHandler implements InvocationHandler {
    User user;

    public UserInvocationHandler(User user) {
        this.user = user;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (method.getName().startsWith("set")) {
            throw new UnsupportedOperationException("You can't change this instance");
        } else {
            return method.invoke(user, args);
        }
    }
}
