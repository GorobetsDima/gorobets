package com.entity.user;

/**
 *
 */
public interface InterUser {

    int getId();

    void setId(int id);


    String getName();

    void setName(String name);

    String getEmail();

    void setEmail(String email);

    String getSurname();

    void setSurname(String surname);

    String getPassword();

    void setPassword(String password);


}
