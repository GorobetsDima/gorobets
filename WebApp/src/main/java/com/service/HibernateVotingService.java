package com.service;

import com.dao.DAO;
import com.dao.exception.DAOException;
import com.entity.Voting;
import com.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 *
 */
public class HibernateVotingService implements VotingService<Voting>{


   DAO<Voting> votingDAO;

    public HibernateVotingService(DAO<Voting> votingDAO) {
        this.votingDAO = votingDAO;
    }



    private static final Logger LOGGER = LogManager.getLogger();


    /**
     * This method  add  a instance of voting to the DB by hibernate.
     *
     * @param voting
     * @return Voting instance
     * @throws ServiceException
     */
    @Override
    public Voting add(Voting voting) throws ServiceException {
        LOGGER.entry(voting);
        Voting addVoting = null;
        if (voting != null) {
            try {
                addVoting = votingDAO.create(voting);
            } catch (DAOException e) {
                LOGGER.error("Exception in Add method at HibernateVotingService class");
                throw new ServiceException(e);
            }
        }
        LOGGER.exit(addVoting);
        return addVoting;
    }
    /**
     * GetAll method -gets all Votings from the DB
     *
     * @return List<Voting> of all users
     * @throws ServiceException
     */
    @Override
    public List<Voting> getAll() throws ServiceException {
        LOGGER.entry("Entry to getAll method at HibernateVotingService");
        try {
            return votingDAO.getAll();
        } catch (DAOException e) {
            LOGGER.error("Exception in getAll method at HibernateVotingService class");
            throw new ServiceException(e);
        }
    }


    /**
     * Remove method - remove Voting from the DB  by their id
     *
     * @param id - id of Voting instance with int type
     */
    @Override
    public void remove(int id) throws ServiceException {
        try {
            votingDAO.remove(id);
        } catch (DAOException e) {
            LOGGER.error("Exception in remove method at HibernateVotingService class");
            throw new ServiceException(e);
        }
    }

}
