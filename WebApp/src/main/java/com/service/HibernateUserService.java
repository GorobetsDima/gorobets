package com.service;

import com.dao.UserDAO;
import com.dao.exception.DAOException;
import com.entity.user.User;
import com.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

/**
 *
 */
public class HibernateUserService implements UserService{

    UserDAO userDAO;

    public HibernateUserService(UserDAO userDAO) {
        this.userDAO = userDAO;
    }



    private static final Logger LOGGER = LogManager.getLogger();


    /**
     * This method  add  a instance of user to the DB by hibernate.
     *
     * @param user
     * @return User instance
     * @throws ServiceException
     */
    @Override
    public User add(User user) throws ServiceException {
        LOGGER.entry(user);
        User addUser = null;
        if (user != null) {
            try {
                addUser = userDAO.create(user);
            } catch (DAOException e) {
                LOGGER.error("Exception in Add method at HibernateUserService class");
                throw new ServiceException(e);
            }
        }
        LOGGER.exit(addUser);
        return addUser;
    }
    /**
     * GetAll method -gets all users from the DB
     *
     * @return List<User> of all users
     * @throws ServiceException
     */
    @Override
    public List<User> getAll() throws ServiceException {
        LOGGER.entry("Entry to getAll method at HibernateUserService");
        try {
            return userDAO.getAll();
        } catch (DAOException e) {
            LOGGER.error("Exception in getAll method at HibernateUserService class");
            throw new ServiceException(e);
        }
    }
    /**
     * This method  get a map with users emails as a key and
     * user surname as a value from the DB.
     *
     * @return Map<String, String> -map with K-email and V- user Surname
     * @throws ServiceException(DAOException)
     */
    @Override
    public Map<String, String> getEmailForEachUser() throws ServiceException {
        LOGGER.entry("Entry to getEmailForEachUser method at HibernateUserService");
        Map<String, String> mapEmailUser = new HashMap<>();
        Set<String> emails = new HashSet<>();

        try {
            for (User user : getAll()) {
                if (user != null) {
                    emails.add(user.getEmail());
                }

            }
            for (String email : emails) {

                if (email != null) {

                    for (User user : getAll()) {
                        if (user != null && email.equals(user.getEmail())) {
                            mapEmailUser.put(email, user.getSurname());
                        }

                    }
                }
            }
        } catch (ServiceException e) {
            LOGGER.error("Exception at getEmailForEachUser method at HibernateUserService");
            throw new ServiceException(e);
        }

        LOGGER.exit(mapEmailUser);
        return mapEmailUser;
    }

    /**
     * Remove method - remove User from the DB  by their id
     *
     * @param id - id of User instance with int type
     */
    @Override
    public void remove(int id) throws ServiceException {
        try {
            userDAO.remove(id);
        } catch (DAOException e) {
            LOGGER.error("Exception in remove method at HibernateUserService class");
            throw new ServiceException(e);
        }
    }

    @Override
    public User getUserByUserEmail(String email) throws ServiceException {
        return null;
    }
}
