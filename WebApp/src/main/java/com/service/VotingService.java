package com.service;

import com.service.exception.ServiceException;

import java.util.List;

/**
 * VotingService - interface that give behavior for actions with storage at service level
 * There is three methods : add(), getAll(), getEmailForEachUser().
 */
public interface VotingService<T> {
    /**
     * Add method - adds Voting to storage
     *
     * @param voting Voting type instance
     * @return Voting voting
     * @throws ServiceException
     */
     T add(T voting) throws ServiceException;

    /**
     * GetAll method - gets all votings from the the storage
     *
     * @return List<Voting> of votings
     * @throws ServiceException
     */
    List<T> getAll() throws ServiceException;


    /**
     * @param id
     * @throws ServiceException
     */
    void remove(int id) throws ServiceException;
}

