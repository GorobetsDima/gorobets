package com.service;

import com.dao.DAO;
import com.dao.exception.DAOException;
import com.entity.VotingOption;
import com.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 *
 */
public class HibernateVotingAnswerService implements VotingService<VotingOption>{


   DAO<VotingOption> votingAnswerDAO;

    public HibernateVotingAnswerService(DAO<VotingOption> votingAnswerDAO) {
        this.votingAnswerDAO = votingAnswerDAO;
    }



    private static final Logger LOGGER = LogManager.getLogger();


    /**
     * This method  add  a instance of votingOption to the DB by hibernate.
     *
     * @param votingOption
     * @return VotingOption instance
     * @throws ServiceException
     */
    @Override
    public VotingOption add(VotingOption votingOption) throws ServiceException {
        LOGGER.entry(votingOption);
        VotingOption addVotingOption = null;
        if (votingOption != null) {
            try {
                addVotingOption = votingAnswerDAO.create(votingOption);
            } catch (DAOException e) {
                LOGGER.error("Exception in Add method at HibernateVotingAnswerService class");
                throw new ServiceException(e);
            }
        }
        LOGGER.exit(addVotingOption);
        return addVotingOption;
    }
    /**
     * GetAll method -gets all VotingAnswers from the DB
     *
     * @return List<VotingOption> of all users
     * @throws ServiceException
     */
    @Override
    public List<VotingOption> getAll() throws ServiceException {
        LOGGER.entry("Entry to getAll method at HibernateVotingAnswerService");
        try {
            return votingAnswerDAO.getAll();
        } catch (DAOException e) {
            LOGGER.error("Exception in getAll method at HibernateVotingAnswerService class");
            throw new ServiceException(e);
        }
    }


    /**
     * Remove method - remove VotingOption from the DB  by their id
     *
     * @param id - id of VotingOption instance with int type
     */
    @Override
    public void remove(int id) throws ServiceException {
        try {
            votingAnswerDAO.remove(id);
        } catch (DAOException e) {
            LOGGER.error("Exception in remove method at HibernateVotingAnswerService class");
            throw new ServiceException(e);
        }
    }

}
