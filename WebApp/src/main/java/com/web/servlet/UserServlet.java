package com.web.servlet;

import com.entity.user.User;
import com.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * UserServlet class-it's a servlet that forward users to registration form
 * It has:
 * doGet and doPost methods for handling requests and giving responses
 */
public class UserServlet extends HttpServlet {
    User user;
    private static final Logger LOGGER = LogManager.getLogger();

    private UserService userService;


    /**
     *
     * @param config
     * @throws ServletException
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        userService = (UserService) config.getServletContext().getAttribute("userService");
    }

    /**
     * Get method that handle request parameter and  give response
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.entry(request, response);
        try {
            request.setAttribute("users", userService.getAll());


        } catch (Exception ex) {
            LOGGER.error("Exception in doGet method at UserServlet");
            throw new ServletException(ex);
        }
        request.getRequestDispatcher("homePage.jsp").forward(request, response);
    }


}
