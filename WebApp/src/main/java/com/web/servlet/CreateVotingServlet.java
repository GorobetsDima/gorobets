package com.web.servlet;

import com.entity.user.User;
import com.entity.Voting;
import com.entity.VotingOption;
import com.google.common.base.Strings;
import com.service.HibernateVotingService;
import com.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 *
 */
@WebServlet(name = "CreateVotingServlet")
public class CreateVotingServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getRootLogger();

    private HibernateVotingService votingService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);


        votingService = (HibernateVotingService) config.getServletContext().getAttribute("votingService");

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String votingName = request.getParameter("votingName");
        String option1 = request.getParameter("option1");
        String option2 = request.getParameter("option2");
        String[] fields = request.getParameterValues("fields[]");
        User author = (User) request.getSession().getAttribute("user");
        Set<VotingOption> options = new HashSet<>();

        options.add(new VotingOption(option1));
        options.add(new VotingOption(option2));

        for (String option : fields) {
            if (!Strings.isNullOrEmpty(option)) {
                options.add(new VotingOption(option));
            }

        }

        Voting voting = new Voting(votingName,author,options);
        try {

            votingService.add(voting);
            LOGGER.info("voting was created!");
        } catch (ServiceException e) {
            LOGGER.error("Exception in CreateVotingsServlet");
            throw new ServletException(e);
        }

        request.getRequestDispatcher("votings/greetings.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.getRequestDispatcher("votings/createVotings.jsp").forward(request, response);
    }
}
