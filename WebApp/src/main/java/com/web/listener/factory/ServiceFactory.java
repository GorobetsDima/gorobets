package com.web.listener.factory;

import com.dao.DAO;
import com.dao.UserDAO;
import com.dao.hibernate.HibernateUserDAO;
import com.dao.hibernate.HibernateVotingAnswerDAO;
import com.dao.hibernate.HibernateVotingDAO;
import com.dao.memory.MemoryUserDAO;
import com.dao.postgesql.PostgresUserDAO;
import com.dao.storage.UserStorage;
import com.db.TransactionManager;
import com.entity.Voting;
import com.entity.VotingOption;
import com.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ServiceConfigurationError;


public class ServiceFactory {

    public static final String MEMORY = "memory";
    public static final String DB = "db";
    public static final String HIBERNATE = "hbm";
    private static final String POSTGRES_DRIVER = "org.postgresql.Driver";

    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * GetUserService method rely on type of storage return appropriate service
     *
     * @param type -it's a type of storage
     * @return UserService type
     */
    public static UserService getUserService(String type) {
        LOGGER.entry(type);
        if (type == null || type.isEmpty()) {
            LOGGER.fatal("Couldn't initialize application. Source type is null or empty");
            throw new IllegalArgumentException();
        }
        if (type.equals(MEMORY)) {
            LOGGER.info("MemoryService initialized");
            return initMemoryService();
        } else if (type.equals(DB)) {
            loadPostgresDriver();
            LOGGER.info("TransactionService initialized");
            return initTransactionalService();
        } else if (type.equals(HIBERNATE)) {
            LOGGER.info("HibernateService initialized");
            return initHibernateUserService();
        } else {
            LOGGER.fatal("Couldn't initialize application with source type {}", type);
            throw new ServiceConfigurationError("Couldn't initialize application with source type [" + type + "]");
        }
    }

    /**
     * LoadPostgresDriver method dose registry for DB Driver
     */
    private static void loadPostgresDriver() {

        try {
            Class.forName(POSTGRES_DRIVER);
        } catch (ClassNotFoundException e) {
            LOGGER.fatal("Couldn't load {} driver", POSTGRES_DRIVER);
            throw new ServiceConfigurationError("Couldn't load" + POSTGRES_DRIVER + "driver");
        }
    }

    /**
     * initMemoryService - initialize memory storage
     *
     * @return UserService
     */
    private static UserService initMemoryService() {
        UserStorage storage = new UserStorage();
        UserDAO userDAO = new MemoryUserDAO(storage);
        return new MemoryUserService(userDAO);
    }

    /**
     * initHibernateService - initialize PostgresDB by hibernate
     *
     * @return UserService
     */
    private static UserService initHibernateUserService() {
        UserDAO userDAO = new HibernateUserDAO();
        return new HibernateUserService(userDAO);
    }
    /**
     * initHibernateService - initialize PostgresDB by hibernate
     *
     * @return UserService
     */
    public static VotingService initHibernateVotingService() {
        DAO<Voting> votingDAO = new HibernateVotingDAO();
        return new HibernateVotingService(votingDAO);
    }
    /**
     * initHibernateService - initialize PostgresDB by hibernate
     *
     * @return UserService
     */
    public static VotingService initHibernateVotingAnsService() {
        DAO<VotingOption> votingAnswerDAO = new HibernateVotingAnswerDAO();
        return new HibernateVotingAnswerService(votingAnswerDAO);
    }
    /**
     * initTransactionalService - initialized PostgresDB
     *
     * @return UserService
     */
    private static UserService initTransactionalService() {
        TransactionManager transactionManager = new TransactionManager();
        UserDAO answerDAO = new PostgresUserDAO();
        return new TransactionalUserService(transactionManager, answerDAO);
    }
}
