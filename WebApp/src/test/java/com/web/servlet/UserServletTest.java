//package com.web.servlet;
//
//import com.entity.user.User;
//import com.service.UserService;
//import junit.framework.TestCase;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.ArgumentCaptor;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.runners.MockitoJUnitRunner;
//
//import javax.servlet.RequestDispatcher;
//import javax.servlet.ServletConfig;
//import javax.servlet.ServletContext;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.ArrayList;
//import java.util.List;
//
//import static org.mockito.Matchers.anyString;
//import static org.mockito.Mockito.verify;
//import static org.mockito.Mockito.when;
//
///**
// *
// */
//@RunWith(MockitoJUnitRunner.class)
//public class UserServletTest extends TestCase {
//    private static final Logger LOGGER = LogManager.getLogger();
//    UserServlet userServlet = new UserServlet();
//    List<User> users = new ArrayList<>();
//
//    @Mock
//    ServletConfig mockConfig = Mockito.mock(ServletConfig.class);
//
//    @Mock
//    ServletContext mockContext = Mockito.mock(ServletContext.class);
//
//    @Mock
//    UserService mockUserService = Mockito.mock(UserService.class);
//
//    @Mock
//    HttpServletRequest mockRequest = Mockito.mock(HttpServletRequest.class);
//
//    @Mock
//    HttpServletResponse mockResponse = Mockito.mock(HttpServletResponse.class);
//
//    @Mock
//    RequestDispatcher mockRequestDispatcher = Mockito.mock(RequestDispatcher.class);
//
//    @Mock
//    Logger logger = Mockito.mock(Logger.class);
//
//    @Before
//    public void setUp() throws Exception {
//        when(mockConfig.getServletContext()).thenReturn(mockContext);
//        when(mockUserService.getAll()).thenReturn(users);
//        when(mockContext.getAttribute("userService")).thenReturn(mockUserService);
//        when(mockRequest.getRequestDispatcher(anyString())).thenReturn(mockRequestDispatcher);
//
//
//    }
////    @Before
////    public void init(ServletConfig config) throws ServletException {
////        userServlet = (UserServlet) config.getServletContext().getAttribute("userServlet");
////
////    }
//
//    @Test
//    public void userServletInitTest() throws ServletException {
//        userServlet.init(mockConfig);
//        verify(mockConfig).getServletContext();
//        verify(mockContext).getAttribute("userService");
//        assertSame(mockUserService,mockContext.getAttribute("userService"));
//    }
//
//    @Test
//    public void userServletGetTest() throws Exception {
//
//        userServlet.doGet(mockRequest, mockResponse);
//
//        verify(logger).entry(mockRequest, mockResponse);
//        ArgumentCaptor<User> argument = ArgumentCaptor.forClass(User.class);
//        User user  = argument.getValue();
//        users.add(user);
//        verify(mockRequest).setAttribute("users",mockUserService.getAll());
//
//
//
//        assertNotNull(mockUserService.getAll());
////        verify(logger).error("Exception in doGet method at UserServlet");
//        verify(mockRequest).getRequestDispatcher("homePage.jsp");
//        verify(mockRequestDispatcher).forward(mockRequest, mockResponse);
//
//
//    }
//
//
//    public void tearDown() throws Exception {
//
//    }
//}