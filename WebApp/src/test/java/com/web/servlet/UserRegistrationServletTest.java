//package com.web.servlet;
//
//
//import com.dto.UserDTO;
//import com.entity.user.User;
//import com.google.gson.Gson;
//import com.service.UserService;
//import com.service.exception.ServiceException;
//import com.validator.Validator;
//import junit.framework.TestCase;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.apache.logging.log4j.util.Strings;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.ArgumentCaptor;
//import org.mockito.Mock;
//import org.mockito.runners.MockitoJUnitRunner;
//
//import javax.servlet.RequestDispatcher;
//import javax.servlet.ServletConfig;
//import javax.servlet.ServletContext;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.util.Map;
//
//import static org.mockito.Matchers.*;
//import static org.mockito.Mockito.verify;
//import static org.mockito.Mockito.when;
//
///**
// *
// */
//@RunWith(MockitoJUnitRunner.class)
//public class UserRegistrationServletTest extends TestCase {
//
//    private static final String SURNAME = "surname";
//    private static final String NAME = "name";
//    private static final String EMAIL = "emailyyy@mail.ru";
//    private static final String PASSWORD = "password";
//    private static final String USER_DTO = "userDTO";
//    private static final String ERRORS = "errors";
//    private static final String CODE = "captcha";
//    private static final String CAPTCHA = "captcha";
//
//
//    UserRegistrationServlet userServlet = new UserRegistrationServlet();
//    private static final Logger LOGGER = LogManager.getRootLogger();
//
//
//    @Mock
//    HttpSession session;
//
//    @Mock
//    HttpServletRequest request;
//    @Mock
//    ServletConfig config;
//    @Mock
//    HttpServletResponse response;
//    @Mock
//    ServletContext context;
//    @Mock
//    UserService userService;
//    @Mock
//    RequestDispatcher dispatcher;
//    @Mock
//    PrintWriter printWriter;
//    @Mock
//    Validator validator;
//
//    Gson gson = new Gson();
//
//
//    @Before
//    public void setUp() throws IOException {
//
//        when(config.getServletContext()).thenReturn(context);
//        when(request.getSession()).thenReturn(session);
//        when(session.getServletContext()).thenReturn(context);
//        when(request.getRequestDispatcher(anyString())).thenReturn(dispatcher);
//        when(context.getAttribute("userService")).thenReturn(userService);
//        when(session.getAttribute("captcha")).thenReturn(CAPTCHA);
//        when(response.getWriter()).thenReturn(printWriter);
//        when(validator.validateEmailAddress(anyString())).thenReturn(true);
//        when(validator.validatePassword(anyString())).thenReturn(true);
//
//
//    }
//
//    @Test
//    public void testServletInit() throws Exception {
//        userServlet.init(config);
//    }
//
//    @Test
//    public void testDoPost() throws ServletException, IOException, ServiceException {
////        userServlet.init(config);
//        mockGetRequestParams(EMAIL, PASSWORD, NAME, SURNAME, CODE, CAPTCHA);
//        UserDTO userDTO = new UserDTO(EMAIL, PASSWORD, NAME, SURNAME);
////        Map<String, String> errors = userServlet.validateForm(userDTO, CAPTCHA, CODE);
//        userServlet.doPost(request, response);
//        mockGetRequestParams(EMAIL, PASSWORD, NAME, SURNAME, CODE, CAPTCHA);
//        ArgumentCaptor<User> argument = ArgumentCaptor.forClass(User.class);
//        verify(userService).add(argument.capture());
//
//        User user = argument.getValue();
//        assertEquals(NAME, user.getName());
//        assertEquals(SURNAME, user.getSurname());
//        assertEquals(EMAIL, user.getEmail());
//        assertEquals(PASSWORD, user.getPassword());
//
//
//        verify(response).setStatus(400);
//        verify(response).setHeader((eq("Content-Type")), eq("application/json"));
//        verify(printWriter).write(gson.toJson(user));
//        when(gson.toJson(user)).thenReturn(anyString());
//
//    }
//
//    @Test(expected = ServletException.class)
//    public void testDoPostWithServiceException() throws ServletException, IOException, ServiceException {
//        mockGetRequestParams(EMAIL, PASSWORD, NAME, SURNAME, CODE, CAPTCHA);
//        when(userService.add(any(User.class))).thenThrow(ServiceException.class);
//        userServlet.doPost(request, response);
//        verify(LOGGER).info(anyString());
//
//
//    }
//
//    @Test(expected = ServletException.class)
//    public void testDoPostWithServiceExceptionOnMemoryService() throws ServletException, IOException, ServiceException {
//
//        mockGetRequestParams(EMAIL, PASSWORD, NAME, SURNAME, CODE, CAPTCHA);
//        when(userService.getEmailForEachUser()).thenThrow(ServiceException.class);
//        userServlet.doPost(request, response);
//    }
//
//    @Test
//    public void testDoPostWithEmptyParamsFromRequest() throws ServletException, IOException {
//        mockGetRequestParams(EMAIL, PASSWORD, NAME, SURNAME, CODE, CAPTCHA);
//        mockGetRequestParams(Strings.EMPTY, Strings.EMPTY, Strings.EMPTY, Strings.EMPTY, Strings.EMPTY, Strings.EMPTY);
//        userServlet.doPost(request, response);
//
//
//    }
//
//    @Test
//    public void testValidateForm() {
//        mockGetRequestParams(EMAIL, PASSWORD, NAME, SURNAME, CODE, CAPTCHA);
//        UserDTO userDTO = new UserDTO(EMAIL, PASSWORD, NAME, SURNAME);
//        Map<String, String> result = userServlet.validateForm(userDTO, CAPTCHA, CODE);
////        when(validator.validateEmailAddress(userDTO.getEmail())).thenReturn(true);
////        when(validator.validateEmailAddress(userDTO.getPassword())).thenReturn(true);
////        mockGetRequestParams(EMAIL, PASSWORD, NAME, SURNAME, CODE, CAPTCHA);
//
//
//        assertTrue(result.isEmpty());
//    }
//
//    @Test
//    public void testValidateFormWithErrors() {
//
//        UserDTO userDTO = new UserDTO(null, null, null, null);
//        Map<String, String> result = userServlet.validateForm(userDTO, CODE, CAPTCHA);
//        assertFalse(result.isEmpty());
//    }
//
//    private void mockGetRequestParams(String name, String surname, String email, String password, String passwordc, String captcha) {
//        when(request.getParameter(EMAIL)).thenReturn(email);
//        when(request.getParameter(PASSWORD)).thenReturn(password);
//        when(request.getParameter(NAME)).thenReturn(name);
//        when(request.getParameter(SURNAME)).thenReturn(surname);
//        when(request.getParameter(CODE)).thenReturn(passwordc);
//        when(request.getSession().getAttribute(CAPTCHA)).thenReturn(captcha);
//        validator.validateEmailAddress(email);
//        validator.validatePassword(password);
//    }
//}