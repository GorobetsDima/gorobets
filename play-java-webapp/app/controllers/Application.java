package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import views.html.responsesCollecting;

public class Application extends Controller {

    public Result index() {
        return ok(responsesCollecting.render("Hi, anonymous user"));

    }
}
