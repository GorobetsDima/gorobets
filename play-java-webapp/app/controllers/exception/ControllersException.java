package controllers.exception;

/**
 * DAOException class- make a own exception by extending a Exception class
 */
public class ControllersException extends Exception {
    /**
     * * Throwable exception that wrapped into own TransactionException exception
     *
     * @param exeption -Throwable exception
     */
    public ControllersException(Throwable exeption) {
        super(exeption);
    }
}
