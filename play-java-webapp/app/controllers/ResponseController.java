package controllers;

import models.Response;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.responses;
import views.html.success;

import javax.persistence.Query;
import java.util.List;

public class ResponseController extends Controller {


    //    @Transactional
    public Result addResponse() {
//        Answer answer = Form.form(Answer.class).bindFromRequest().get();
//        JPA.em().persist(answer);


        //TODO перход на страничку благодарности после добавления response
        return ok(success.render("Thank you for submitting your data!"));
    }


    @Transactional
    public Result getResponses() {
        Query query = JPA.em().createQuery("select e from Response e");
        List<Response> responseList = query.getResultList();
        return ok(responses.render(responseList));
    }

    public Result reset() {
        return redirect("/");
    }

}
