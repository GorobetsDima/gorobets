package controllers;

import models.Admin;
import play.data.Form;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.logIn;
import views.html.responsesCollecting;

import javax.persistence.Query;
import java.util.List;

public class LogInController extends Controller {


    public Result logIn() {
        return ok(logIn.render());
    }


    public Result logOut() {
        return ok(responsesCollecting.render(""));
    }

    @Transactional
    public Result getLogIn() {
        Admin admin = Form.form(Admin.class).bindFromRequest().get();
        System.out.println(admin);

        Query query = JPA.em().createQuery("select e from Admin e");

        List<Admin> admins = query.getResultList();
        System.out.println(admins);
        for (Admin adminPersist : admins) {
            if (adminPersist.getPassword().equalsIgnoreCase(admin.getPassword()) && adminPersist.getName().equalsIgnoreCase(admin.getName())) {
                System.out.println(" Должно быть все ок!");
                return redirect("/fields");
            }

        }
//TODO валидацию формы логирования
        System.out.println("Что-то не так!");
        return redirect("/logIn");
    }


}
