package controllers;

import controllers.exception.ControllersException;
import dao.DAOImpl.JPAFieldDAO;
import dao.FieldDAO;
import models.Field;
import play.Logger;
import play.data.DynamicForm;
import play.data.Form;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import service.JPAFieldService;
import service.Service;
import service.exception.ServiceException;
import views.html.createEditField;
import views.html.fieldsTable;

import java.util.List;

public class FieldController extends Controller {
    String error = "";
    FieldDAO fieldDAO = new JPAFieldDAO();
    Service fieldService = new JPAFieldService(fieldDAO);

    public Result createField() {
        Field field = new Field();
        return ok(createEditField.render(field, error));
    }

    @Transactional
    public Result addField() throws ControllersException {
        String error;
        Field empty = new Field();
        Field field = Form.form(Field.class).bindFromRequest().get();
        //TODO разобраться как принят JSON с AJAX запроса
        try {
            List<Field> fields = fieldService.getAll();
            for (Field filedPersist : fields) {
                if (filedPersist.getLable().equalsIgnoreCase(field.getLable())) {
                    error = "This name exist";
                    return ok(createEditField.render(empty, error));
                }


            }
            fieldService.add(field);
        } catch (ServiceException e) {
            Logger.error("ControllersException during addField method at FieldController", e);
            throw new ControllersException(e);
        }
        return redirect("/fields");
    }

    @Transactional
    public Result deleteField() throws ControllersException {
        DynamicForm requestData = Form.form().bindFromRequest();
        String fieldId = requestData.get("id");
        Integer id = Integer.valueOf(fieldId);
        try {
            fieldService.remove(id);
        } catch (ServiceException e) {
            Logger.error("ControllersException during deleteField method at FieldController", e);
            throw new ControllersException(e);
        }
        return redirect("/fields");
    }

    @Transactional
    public Result getFields() throws ControllersException {
        String checkedR = null;
        String checkedA = null;
        List<Field> fields;

        try {
            fields = (List<Field>) fieldService.getAll();
            for (Field field : fields) {
                if (field.isActive()) {
                    checkedA = "checked";
                } else if (field.isRequired()) {
                    checkedR = "checked";
                } else {
                    checkedA = "";
                    checkedR = "";
                }
            }
        } catch (ServiceException e) {
            Logger.error("ControllersException during getFields method at FieldController", e);
            throw new ControllersException(e);
        }

        return ok(fieldsTable.render(fields, checkedR, checkedA));
    }

    @Transactional
    public Result editField() throws ControllersException {
        String error = "";
        Field persistField;
        DynamicForm requestData = Form.form().bindFromRequest();
        String fieldId = requestData.get("id");
        Integer id = Integer.valueOf(fieldId);
        try {
            persistField = (Field) fieldService.get(id);
        } catch (ServiceException e) {
            Logger.error("ControllersException during editField method at FieldController", e);
            throw new ControllersException(e);
        }
        return ok(createEditField.render(persistField, error));
    }
}
