package dao.DAOImpl;

import dao.DAO;
import dao.exception.DAOException;
import models.Response;
import org.hibernate.HibernateException;
import play.Logger;
import play.db.jpa.JPA;

import javax.persistence.Query;
import java.util.List;

/**
 *
 */
public class JPAPesponseDAO implements DAO<Response> {

    /**
     * *
     * Create method - adds Response to the DB
     *
     * @param response - Response instance
     * @return Response instance from DB
     * @throws DAOException
     */
    @Override
    public Response create(Response response) throws DAOException {

        Response persistedResponse = null;
        try {
            JPA.em().persist(response);
            List<Response> responses = getAll();
            for (Response persisAdmin : responses) {
                if (persisAdmin.getAnswers().equals(response.getAnswers())) {
                    persistedResponse = persisAdmin;
                }
            }
        } catch (HibernateException he) {
            Logger.error("HibernateException during response create method", he);
            throw new DAOException(he);
        }
        return persistedResponse;
    }

    /**
     * Get method - get Response from the DB storage by Response id.
     *
     * @param id - id of Response instance with int type
     * @return Response instance from DB
     * @throws DAOException
     */
    @Override
    public Response get(Integer id) throws DAOException {
        Response persistedResponse = null;
        try {

            List<Response> responseList = getAll();
            for (Response response : responseList) {
                if (response.getId().equals(id)) {
                    persistedResponse = response;
                }
            }

        } catch (HibernateException he) {
            Logger.error("HibernateException during response get method", he);
            throw new DAOException(he);
        }
        return persistedResponse;
    }

    /**
     * Update method - update Response at the DB storage
     *
     * @param response - Response instance
     * @return Response instance from DB
     * @throws DAOException
     */
    @Override
    public void update(Response response) throws DAOException {
        try {
            JPA.em().refresh(response);
        } catch (HibernateException he) {
            Logger.error("HibernateException during response update method", he);
            throw new DAOException(he);
        }

    }

    /**
     * Remove method - remove Response from the DB storage by Response id
     *
     * @param id - id of Response instance with int type
     * @throws DAOException
     */
    @Override
    public void remove(Integer id) throws DAOException {

        try {

            Response response = get(id);

            JPA.em().remove(response);

        } catch (HibernateException he) {
            Logger.error("HibernateException during response remove method", he);
            throw new DAOException(he);
        }

    }

    /**
     * GetAll method - get List of Response from the DB storage
     *
     * @return List<T> -list with Responses
     * @throws DAOException
     */
    @Override
    public List<Response> getAll() throws DAOException {

        List<Response> responsesList = null;

        try {
            Query query = JPA.em().createQuery("select e from Response e");
            responsesList = query.getResultList();
        } catch (HibernateException he) {
            Logger.error("HibernateException during response getAll method", he);
            throw new DAOException(he);
        }

        return responsesList;
    }



}
