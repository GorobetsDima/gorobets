package dao.DAOImpl;


import com.google.common.base.Strings;
import dao.FieldDAO;
import dao.exception.DAOException;
import models.Field;
import org.hibernate.HibernateException;
import play.Logger;
import play.data.DynamicForm;
import play.data.Form;
import play.db.jpa.JPA;

import javax.persistence.Query;
import java.util.List;

/**
 *
 */
public class JPAFieldDAO implements FieldDAO {


    /**
     * *
     * Create method - adds Field to the DB
     *
     * @param field - Field instance
     * @return Field instance from DB
     * @throws DAOException
     */
    @Override

    public Field create(Field field) throws DAOException {
        Field persistedField = null;
        try {

            DynamicForm requestData = Form.form().bindFromRequest();
            String required = requestData.get("required");
            String active = requestData.get("active");
            System.out.println(required +" "+active);
            if (!Strings.isNullOrEmpty(required)) {
                field.setRequired(true);
            } else if(!Strings.isNullOrEmpty(active)){
                field.setActive(true);
            }else{
                field.setRequired(false);
                field.setActive(false);
            }
            JPA.em().persist(field);
            List<Field> fields = getAll();
            for (Field persisField : fields) {
                if (persisField.getLable().equalsIgnoreCase(persisField.getLable())) {
                    persistedField = persisField;
                }
            }
        } catch (HibernateException he) {
            Logger.error("HibernateException during field create method", he);
            throw new DAOException(he);
        }
        return persistedField;
    }

    /**
     * Get method - get Field from the DB storage by Field id.
     *
     * @param id - id of Field instance with int type
     * @return Field instance from DB
     * @throws DAOException
     */
    @Override
    public Field get(Integer id) throws DAOException {
        Field persistedField = null;
        try {

            List<Field> fieldList = getAll();
            for (Field field : fieldList) {
                if (field.getId().equals(id)) {
                    persistedField = field;
                }
            }

        } catch (HibernateException he) {
            Logger.error("HibernateException during field get method", he);
            throw new DAOException(he);
        }
        return persistedField;
    }

    /**
     * Update method - update Field at the DB storage
     *
     * @param field - Field instance
     * @return Field instance from DB
     * @throws DAOException
     */
    @Override
    public void update(Field field) throws DAOException {
        try {
            JPA.em().refresh(field);
        } catch (HibernateException he) {
            Logger.error("HibernateException during field update method", he);
            throw new DAOException(he);
        }

    }

    /**
     * Remove method - remove Field from the DB storage by Field id
     *
     * @param id - id of Field instance with int type
     * @throws DAOException
     */
    @Override
    public void remove(Integer id) throws DAOException {

        try {

            Field field = get(id);

            JPA.em().remove(field);

        } catch (HibernateException he) {
            Logger.error("HibernateException during field remove method", he);
            throw new DAOException(he);
        }

    }

    /**
     * GetAll method - get List of Field from the DB storage
     *
     * @return List<T> -list with Fields
     * @throws DAOException
     */
    @Override

    public List<Field> getAll() throws DAOException {

        List<Field> fieldsList = null;

        try {
            Query query = JPA.em().createQuery("select e from Field e");
            fieldsList = query.getResultList();


        } catch (HibernateException he) {
            Logger.error("HibernateException during field getAll method", he);
            throw new DAOException(he);
        }

        return fieldsList;
    }


}
