package dao.DAOImpl;

import dao.DAO;
import dao.exception.DAOException;
import models.Admin;
import org.hibernate.HibernateException;
import play.Logger;
import play.db.jpa.JPA;

import javax.persistence.Query;
import java.util.List;

/**
 *
 */
public class JPAAdminDAO implements DAO<Admin> {


    /**
     * *
     * Create method - adds Admin to the DB
     *
     * @param admin - Admin instance
     * @return Admin instance from DB
     * @throws DAOException
     */
    @Override
    public Admin create(Admin admin) throws DAOException {

        Admin persistedAdmin = null;
        try {
            JPA.em().persist(admin);
            List<Admin> admins = getAll();
            for (Admin persisAdmin : admins) {
                if (persisAdmin.getPassword().equalsIgnoreCase(admin.getPassword())) {
                    persistedAdmin = persisAdmin;
                }
            }
        } catch (HibernateException he) {
            Logger.error("HibernateException during admin create method", he);
            throw new DAOException(he);
        }
        return persistedAdmin;
    }

    /**
     * Get method - get Admin from the DB storage by Admin id.
     *
     * @param id - id of Admin instance with int type
     * @return Admin instance from DB
     * @throws DAOException
     */
    @Override
    public Admin get(Integer id) throws DAOException {
        Admin persistedAdmin = null;
        try {

            List<Admin> adminList = getAll();
            for (Admin admin : adminList) {
                if (admin.getId().equals(id)) {
                    persistedAdmin = admin;
                }
            }

        } catch (HibernateException he) {
            Logger.error("HibernateException during admin get method", he);
            throw new DAOException(he);
        }
        return persistedAdmin;
    }

    /**
     * Update method - update Admin at the DB storage
     *
     * @param admin - Admin instance
     * @return Admin instance from DB
     * @throws DAOException
     */
    @Override
    public void update(Admin admin) throws DAOException {
        try {
            JPA.em().refresh(admin);
        } catch (HibernateException he) {
            Logger.error("HibernateException during admin update method", he);
            throw new DAOException(he);
        }

    }

    /**
     * Remove method - remove Admin from the DB storage by Admin id
     *
     * @param id - id of Admin instance with int type
     * @throws DAOException
     */
    @Override
    public void remove(Integer id) throws DAOException {

        try {

            Admin admin = get(id);

            JPA.em().remove(admin);

        } catch (HibernateException he) {
            Logger.error("HibernateException during admin remove method", he);
            throw new DAOException(he);
        }

    }

    /**
     * GetAll method - get List of Admin from the DB storage
     *
     * @return List<T> -list with Admins
     * @throws DAOException
     */
    @Override
    public List<Admin> getAll() throws DAOException {

        List<Admin> adminsList = null;

        try {
            Query query = JPA.em().createQuery("select e from Admin e");
            adminsList = query.getResultList();
        } catch (HibernateException he) {
            Logger.error("HibernateException during admin getAll method", he);
            throw new DAOException(he);
        }

        return adminsList;
    }



}
