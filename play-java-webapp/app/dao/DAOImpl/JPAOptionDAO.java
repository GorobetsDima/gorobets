package dao.DAOImpl;

import dao.DAO;
import dao.exception.DAOException;
import models.Option;
import org.hibernate.HibernateException;
import play.Logger;
import play.db.jpa.JPA;

import javax.persistence.Query;
import java.util.List;

/**
 *
 */
public class JPAOptionDAO implements DAO<Option> {

    /**
     * *
     * Create method - adds Option to the DB
     *
     * @param option - Option instance
     * @return Option instance from DB
     * @throws DAOException
     */
    @Override
    public Option create(Option option) throws DAOException {

        Option persistedOption = null;
        try {
            JPA.em().persist(option);
            List<Option> answers = getAll();
            for (Option persisOption : answers) {
                if (persisOption.getValue().equalsIgnoreCase(option.getValue())) {
                    persistedOption = persisOption;
                }
            }
        } catch (HibernateException he) {
            Logger.error("HibernateException during option create method", he);
            throw new DAOException(he);
        }
        return persistedOption;
    }

    /**
     * Get method - get Option from the DB storage by Option id.
     *
     * @param id - id of Option instance with int type
     * @return Option instance from DB
     * @throws DAOException
     */
    @Override
    public Option get(Integer id) throws DAOException {
        Option persistedOption = null;
        try {

            List<Option> fieldList = getAll();
            for (Option option : fieldList) {
                if (option.getId().equals(id)) {
                    persistedOption = option;
                }
            }

        } catch (HibernateException he) {
            Logger.error("HibernateException during option get method", he);
            throw new DAOException(he);
        }
        return persistedOption;
    }

    /**
     * Update method - update Option at the DB storage
     *
     * @param option - Option instance
     * @return Option instance from DB
     * @throws DAOException
     */
    @Override
    public void update(Option option) throws DAOException {
        try {
            JPA.em().refresh(option);
        } catch (HibernateException he) {
            Logger.error("HibernateException during option update method", he);
            throw new DAOException(he);
        }

    }

    /**
     * Remove method - remove Option from the DB storage by Option id
     *
     * @param id - id of Option instance with int type
     * @throws DAOException
     */
    @Override
    public void remove(Integer id) throws DAOException {

        try {

            Option option = get(id);

            JPA.em().remove(option);

        } catch (HibernateException he) {
            Logger.error("HibernateException during option remove method", he);
            throw new DAOException(he);
        }

    }

    /**
     * GetAll method - get List of Option from the DB storage
     *
     * @return List<T> -list with Options
     * @throws DAOException
     */
    @Override
    public List<Option> getAll() throws DAOException {

        List<Option> optionsList;

        try {
            Query query = JPA.em().createQuery("select e from Option e");
            optionsList = query.getResultList();
        } catch (HibernateException he) {
            Logger.error("HibernateException during option getAll method", he);
            throw new DAOException(he);
        }

        return optionsList;
    }



}
