package dao.DAOImpl;

import dao.DAO;
import dao.exception.DAOException;
import models.Answer;
import org.hibernate.HibernateException;
import play.Logger;
import play.db.jpa.JPA;

import javax.persistence.Query;
import java.util.List;

/**
 *
 */
public class JPAAnswerDAO implements DAO<Answer> {

    /**
     * *
     * Create method - adds Answer to the DB
     *
     * @param answer - Answer instance
     * @return Answer instance from DB
     * @throws DAOException
     */
    @Override
    public Answer create(Answer answer) throws DAOException {

        Answer persistedAnswer = null;
        try {
            JPA.em().persist(answer);
            List<Answer> answers = getAll();
            for (Answer persisAnswer : answers) {
                if (persisAnswer.getValue().equalsIgnoreCase(persisAnswer.getValue())) {
                    persistedAnswer = persisAnswer;
                }
            }
        } catch (HibernateException he) {
            Logger.error("HibernateException during answer create method", he);
            throw new DAOException(he);
        }
        return persistedAnswer;
    }

    /**
     * Get method - get Answer from the DB storage by Answer id.
     *
     * @param id - id of Answer instance with int type
     * @return Answer instance from DB
     * @throws DAOException
     */
    @Override
    public Answer get(Integer id) throws DAOException {
        Answer persistedAnswer = null;
        try {

            List<Answer> fieldList = getAll();
            for (Answer answer : fieldList) {
                if (answer.getId().equals(id)) {
                    persistedAnswer = answer;
                }
            }

        } catch (HibernateException he) {
            Logger.error("HibernateException during answer get method", he);
            throw new DAOException(he);
        }
        return persistedAnswer;
    }

    /**
     * Update method - update Answer at the DB storage
     *
     * @param answer - Answer instance
     * @return Answer instance from DB
     * @throws DAOException
     */
    @Override
    public void update(Answer answer) throws DAOException {
        try {
            JPA.em().refresh(answer);
        } catch (HibernateException he) {
            Logger.error("HibernateException during answer update method", he);
            throw new DAOException(he);
        }

    }

    /**
     * Remove method - remove Field from the DB storage by Field id
     *
     * @param id - id of Field instance with int type
     * @throws DAOException
     */
    @Override
    public void remove(Integer id) throws DAOException {

        try {

            Answer answer = get(id);

            JPA.em().remove(answer);

        } catch (HibernateException he) {
            Logger.error("HibernateException during field remove method", he);
            throw new DAOException(he);
        }

    }

    /**
     * GetAll method - get List of Answer from the DB storage
     *
     * @return List<T> -list with Answers
     * @throws DAOException
     */
    @Override
    public List<Answer> getAll() throws DAOException {

        List<Answer> answersList;

        try {
            Query query = JPA.em().createQuery("select e from Answer e");
            answersList = query.getResultList();
        } catch (HibernateException he) {
            Logger.error("HibernateException during answer getAll method", he);
            throw new DAOException(he);
        }

        return answersList;
    }


}
