package dao;

import models.Field;

/**
 * DAO - data access object interface for connection with DB.
 *
 *
 */
public interface FieldDAO extends DAO<Field> {

}
