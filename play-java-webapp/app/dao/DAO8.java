package dao;

import dao.exception.DAOException;

import play.db.jpa.JPA;

import java.util.List;

/**
 * DAO - data access object interface for connection with DB.
 *
 * @param <T> - generic type of object
 */
@FunctionalInterface
public interface DAO8<T> {


    /**
     * GetAll method - get all objects from the storage
     *
     * @return List<T> -list with objects
     * @throws DAOException
     */
    List<T> getAll() throws DAOException;

    /**
     * Create method - adds object to the storage
     *
     * @param entity - object instance with <T> type
     * @return <T> type
     * @throws DAOException
     */
    default void create(T entity) throws DAOException {

        JPA.em().persist(entity);

    }

    /**
     * Get method - get object from the storage
     *
     * @param id - id of object instance with int type
     * @return <T> type
     * @throws DAOException
     */
//    default T get(Integer id) throws DAOException {
//
//    }

    /**
     * Update method - update object at the storage
     *
     * @param entity - object instance with <T> type
     * @return <T> type
     * @throws DAOException
     */
    default void update(T entity) throws DAOException {

    }

    /**
     * Remove method - remove object from the storage
     *
     * @param id - id of object instance with int type
     * @return <T> type
     * @throws DAOException
     */
    default void remove(Integer id) throws DAOException {

    }


}
