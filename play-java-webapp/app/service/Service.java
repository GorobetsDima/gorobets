package service;


import service.exception.ServiceException;

import java.util.List;


/**
 * Service - interface that give behavior for actions with storage at service level
 * There is three methods : add(), getAll(), getEmailForEachUser().
 */
public interface Service<T> {

    /**
     * Add method - adds T to storage
     *
     * @param entity T type instance
     * @return T field
     * @throws ServiceException
     */
    T add(T entity) throws ServiceException;

    /**
     * Det method - gets T from storage
     *
     * @param id id of instance
     * @return T field
     * @throws ServiceException
     */
    T get(Integer id) throws ServiceException;

    /**
     * GetAll method -gets all entities from the the storage
     *
     * @return List<T> of entities
     * @throws ServiceException
     */
    List<T> getAll() throws ServiceException;

    /**
     * @param id
     * @throws ServiceException
     */
    void remove(int id) throws ServiceException;



    /**
     * Update method - update object at the storage
     *
     * @param entity - object instance with <T> type
     * @return <T> type
     * @throws ServiceException
     */
    void update(T entity) throws ServiceException;
}
