package service;


import dao.FieldDAO;
import dao.exception.DAOException;
import models.Field;
import play.Logger;
import service.exception.ServiceException;

import java.util.List;

/**
 *
 */
public class JPAFieldService implements Service<Field> {

    FieldDAO fieldDAO;

    public JPAFieldService(FieldDAO fieldDAO) {
        this.fieldDAO = fieldDAO;
    }


    /**
     * This method  add  a instance of field to the DB by hibernate.
     *
     * @param field
     * @return Field instance
     * @throws ServiceException
     */
    @Override
    public Field add(Field field) throws ServiceException {

        Field addField = null;
        if (field != null) {
            try {
                addField = fieldDAO.create(field);
            } catch (DAOException e) {
                Logger.error("Exception in Add method at JPAFieldService class");
                throw new ServiceException(e);
            }
        }

        return addField;
    }
    /**
     * This method  add  a instance of field to the DB by hibernate.
     *
     * @param id
     * @return Field instance
     * @throws ServiceException
     */
    @Override
    public Field get(Integer id) throws ServiceException {

        Field addField = null;
        if (id != 0) {
            try {
                addField = fieldDAO.get(id);
            } catch (DAOException e) {
                Logger.error("Exception in Add method at JPAFieldService class");
                throw new ServiceException(e);
            }
        }

        return addField;
    }
    /**
     * GetAll method -gets all fields from the DB
     *
     * @return List<Field> of all fields
     * @throws ServiceException
     */
    @Override
    public List<Field> getAll() throws ServiceException {
        try {
            return fieldDAO.getAll();
        } catch (DAOException e) {
            Logger.error("Exception in getAll method at JPAFieldService class");
            throw new ServiceException(e);
        }
    }



    /**
     * Remove method - remove Field from the DB  by their id
     *
     * @param id - id of Field instance with int type
     */
    @Override
    public void remove(int id) throws ServiceException {
        try {
            fieldDAO.remove(id);
        } catch (DAOException e) {
            Logger.error("Exception in remove method at JPAFieldService class");
            throw new ServiceException(e);
        }
    }

    /**
     * Update method - update object at the storage
     *
     * @param entity - object instance with <Field> type
     * @return <Field> type
     * @throws ServiceException
     */
    @Override
    public void update(Field entity) throws ServiceException {
        try {
            fieldDAO.update(entity);
        } catch (DAOException e) {
            Logger.error("Exception in remove method at JPAFieldService class");
            throw new ServiceException(e);
        }
    }

}
