package models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "fields"/*, uniqueConstraints = @UniqueConstraint(columnNames = "LABLE")*/)
public class Field {


    private Integer id;

    private String lable;

    private FieldType fieldType;

    private boolean required;


    private boolean active;

    private Set<Option> options = new HashSet<>(0);

    private Set<Answer> answers = new HashSet<>(0);


    public Field() {
    }


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "FIELD_ID", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    @Column(name = "LABLE", unique = true, nullable = false)
    public String getLable() {
        return lable;
    }

    public void setLable(String lable) {
        this.lable = lable;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "FIELD_TYPE", unique = false, nullable = false)
    public FieldType getFieldType() {
        return fieldType;
    }

    public void setFieldType(FieldType fieldType) {
        this.fieldType = fieldType;
    }


    @Column(name = "REQUIRED", unique = false, nullable = true, length = 10)
    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }


    @Column(name = "ACTIVE", unique = false, nullable = true, length = 10)
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean isActive) {
        this.active = isActive;
    }


    @OneToMany(fetch = FetchType.EAGER, mappedBy = "field")
    public Set<Option> getOptions() {
        return options;
    }

    public void setOptions(Set<Option> options) {
        this.options = options;
    }


    @OneToMany(fetch = FetchType.EAGER, mappedBy = "field")
    public Set<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(Set<Answer> answers) {
        this.answers = answers;
    }

}
