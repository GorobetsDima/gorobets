package models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 *
 *
 */
@Entity
@Table(name = "answers", uniqueConstraints = @UniqueConstraint(columnNames = "VALUE"))
public class Answer {

    private Integer id;

    private String value;//это тип поля которое заполняет сам юзер, которое не имеет вариантов ответа!(дата , текст, большой текст,)

    private Field field;

    private Response response;


    private Set<Option> options = new HashSet<>(0);

    public Answer() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "ANSWER_ID", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "FIELD_ID", nullable = false)
    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }


    @Column(name = "VALUE", unique = false, nullable = false)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "RESPONSE_ID", nullable = false)
    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }


    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "answer_option",  joinColumns = {
            @JoinColumn(name = "ANSWER_ID", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "OPTION_ID",
                    nullable = false, updatable = false) })
    public Set<Option> getOptions() {
        return options;
    }

    public void setOptions(Set<Option> options) {
        this.options = options;
    }

}
