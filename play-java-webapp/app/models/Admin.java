package models;

import play.data.validation.ValidationError;
import play.data.validation.Constraints.Required;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


/**
 *
 */
@Entity
@Table(name = "admins", uniqueConstraints =@UniqueConstraint(columnNames = "PASSWORD"))
public class Admin {

    private  Integer id;

    @Required
    private String name;
    @Required
    private String surname;

    @Required
    private String password;

    public Admin() {
    }

    public List<ValidationError> validate() {
        List<ValidationError> errors = new ArrayList<ValidationError>();
        //TODO валидацию админ формы через плэй смотреть туториал ....
//        if (Admin.byEmail(email) == null) {
//            errors.add(new ValidationError("email", "This e-mail is already registered."));
//        }
        return errors.isEmpty() ? null : errors;

    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "ADMIN_ID", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    @Column(name = "NAME", unique = false, nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @Column(name = "SURNAME", unique = false, nullable = false)
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Column(name = "PASSWORD", unique = false, nullable = false)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
