package models;

import javax.persistence.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 *
 */
@Entity
@Table(name = "responses")
public class Response {

    private  Integer id;

    private Date time;// определиться с типом времени

    private Set<Answer> answers = new HashSet<>(0);

    public Response() {
    }


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "PESPONSE_ID", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "TIME", unique = true, nullable = false)
    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "response")
    public Set<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(Set<Answer> answers) {
        this.answers = answers;
    }
}
