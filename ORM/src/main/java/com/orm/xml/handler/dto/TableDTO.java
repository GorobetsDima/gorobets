package com.orm.xml.handler.dto;

import java.util.List;

public class TableDTO {

	private String name;
	private List<ColumnDTO> columns;

	public TableDTO(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ColumnDTO> getColumns() {
		return columns;
	}

	public void setColumns(List<ColumnDTO> columns) {
		this.columns = columns;
	}

	@Override
	public String toString() {
		return "TableDTO [name=" + name + ", columns=" + columns + "]";
	}
}
