package com.orm.xml.handler;

import com.orm.xml.handler.dto.ColumnDTO;
import com.orm.xml.handler.dto.TableDTO;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;


/**
 * This class uses Java Reflection API to transform class fields and annotation
 * attributes to 'create table' sql query. There are a lot of hardcoded
 * properties and ugly manipulations with strings, I didn't want to use
 * third-party libraries, there is just a raw java.
 */
public class EntityHandler {

    {
        try {
            Class.forName(JDBC_DRIVER);
        } catch (ClassNotFoundException e) {
            System.err.println(e.toString());
        }
    }

    private static final String JDBC_DRIVER = "org.postgresql.Driver";
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/usersdb";
    private static final String USER = "postgres";
    private static final String PASSWORD = "programmer88";

    private final Map<String, String> types = new HashMap<String, String>() {
        {
            put("java.lang.Integer", "integer");
            put("java.lang.String", "VARCHAR(40)");
            put("int", "integer");
        }
    };

    public void createTable(Class<?> entity) {
        TableDTO table = buildTableDTO(entity);
        createTable(table);
    }

    private TableDTO buildTableDTO(Class<?> entityClass) {


        SAXParserFactory parserFactor = SAXParserFactory.newInstance();
        SAXParser parser = null;
        try {
            parser = parserFactor.newSAXParser();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }

        SAXHandler sax = new SAXHandler();

        try {
            if (parser != null) {
//                File file = new File("com/orm/example/xml/mapping.xml");
//                InputStream inputStream = new FileInputStream(file);
                parser.parse(ClassLoader.getSystemResourceAsStream("C:\\Users\\master\\IdeaProjects\\Gorobets\\ORM\\mapping.xml"), sax);
//                parser.parse(inputStream, sax);
            }
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        TableDTO tableDTO = null;

        if (entityClass.toString().equals("Dog")) {
            tableDTO = sax.dogTable;
        }
        if (entityClass.toString().equals("Person")) {
            tableDTO = sax.personTable;
        }

        return tableDTO;
    }

    private void createTable(TableDTO table) {
        try {
            Connection conn = DriverManager.getConnection(DB_URL, USER, PASSWORD);
            Statement stmt = conn.createStatement();
            String sql = getCreateTableQuery(table);
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            System.err.println(e.toString());
        }
    }

    private String getCreateTableQuery(TableDTO table) {
        StringBuilder columns = new StringBuilder();
        for (ColumnDTO column : table.getColumns()) {
            if (column.getName().contains("id")) {
                columns.append(column.getName()).append(" ").append(types.get(column.getType()))
                        .append(" ").append("PRIMARY KEY").append(", ");
            } else {
                columns.append(column.getName()).append(" ").append(types.get(column.getType())).append(", ");
            }

        }

        columns.deleteCharAt(columns.length() - 2);

        StringBuilder sqlQuery = new StringBuilder("CREATE TABLE ").append(table.getName()).append("( ").append(columns)
                .append(");");
        System.out.println("generated sqlQuery: " + sqlQuery);
        return sqlQuery.toString();
    }
}
