package com.orm.xml.handler;

import com.orm.xml.handler.dto.ColumnDTO;
import com.orm.xml.handler.dto.TableDTO;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;





    /**
     * 25
     * The Handler for SAX Events.
     * 26
     */
    public class SAXHandler extends DefaultHandler {

        public List<ColumnDTO> dogColumns = new ArrayList<>();
        public List<ColumnDTO> personColumns = new ArrayList<>();

        TableDTO dogTable = null;
        TableDTO personTable = null;


        @Override
        //Triggered when the start of tag is found.

        public void startElement(String uri, String localName,
                                 String qName, Attributes attributes)
                throws SAXException {
            if (qName.equals("dog")) {
                dogTable = new TableDTO(attributes.getValue("table"));
                switch (qName) {
                    case "id":
                        dogColumns.add(new ColumnDTO(attributes.getValue("column"), "java.lang.Integer"));
                        break;
                    case "name":
                        dogColumns.add(new ColumnDTO(attributes.getValue("column"), "java.lang.String"));
                        break;
                    case "age":
                        dogColumns.add(new ColumnDTO(attributes.getValue("column"), "int"));
                        break;

                }
//                dogTable.setColumns(dogColumns);
            }
            if (qName.equals("person")) {
                personTable = new TableDTO(attributes.getValue("table"));
                switch (qName) {
                    case "id":
                        personColumns.add(new ColumnDTO(attributes.getValue("column"), "java.lang.Integer"));
                        break;
                    case "name":
                        personColumns.add(new ColumnDTO(attributes.getValue("column"), "java.lang.String"));
                        break;
                    case "email":
                        personColumns.add(new ColumnDTO(attributes.getValue("column"), "java.lang.String"));
                        break;
                    case "phoneNumber":
                        personColumns.add(new ColumnDTO(attributes.getValue("column"), "java.lang.String"));
                        break;
                    case "age":
                        personColumns.add(new ColumnDTO(attributes.getValue("column"), "java.lang.String"));
                        break;

                }
//                dogTable.setColumns(personColumns);

            }


        }


        @Override
        public void endElement(String uri, String localName,

                               String qName) throws SAXException {

            switch (qName) {

                //Add the employee to list once end tag is found

                case "dog":

                    dogTable.setColumns(dogColumns);

                    break;

                //For all other end tags the employee has to be updated.

                case "person":
                    personTable.setColumns(personColumns);

                    break;

            }

        }


//        @Override
//
//        public void characters(char[] ch, int start, int length)
//
//                throws SAXException {
//
//            content = String.copyValueOf(ch, start, length).trim();
//
//        }


    }





