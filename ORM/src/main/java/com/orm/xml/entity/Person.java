package com.orm.xml.entity;


public class Person {

	private Integer personId;

	private String name;

	private String email;

	private Integer age;

	private String phoneNumber;

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return "Person{" +
				"personId=" + personId +
				", name='" + name + '\'' +
				", email='" + email + '\'' +
				", age=" + age +
				", phoneNumber='" + phoneNumber + '\'' +
				'}';
	}
}
