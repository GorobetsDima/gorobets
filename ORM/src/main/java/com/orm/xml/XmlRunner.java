package com.orm.xml;


import com.orm.xml.entity.Dog;
import com.orm.xml.entity.Person;
import com.orm.xml.handler.EntityHandler;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class XmlRunner {
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {


		EntityHandler handler = new EntityHandler();
		handler.createTable(Dog.class);
		handler.createTable(Person.class);
	}
}
