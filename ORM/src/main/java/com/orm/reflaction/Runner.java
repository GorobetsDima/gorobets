package com.orm.reflaction;


import com.orm.reflaction.entity.Dog;
import com.orm.reflaction.entity.Person;
import com.orm.reflaction.handler.EntityHandler;

public class Runner {
	public static void main(String[] args) {
		EntityHandler handler = new EntityHandler();
		handler.createTable(Dog.class);
		handler.createTable(Person.class);

	}
}
