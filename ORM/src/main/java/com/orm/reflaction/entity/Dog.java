package com.orm.reflaction.entity;


import com.orm.reflaction.annotation.Column;
import com.orm.reflaction.annotation.Table;

@Table(name = "DOGS")
public class Dog {

	@Column(name = "dog_id")
	private Integer id;
	
	@Column(name = "name")
	private String name;

	@Column(name = "age")
	private int age;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
}
