package com.orm.reflaction.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Actually HIBERNATE uses JPA annotations, but I have written this one to
 * demonstrate how to implement custom annotation.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE) // for class level
public @interface Table {

	String name();
}
