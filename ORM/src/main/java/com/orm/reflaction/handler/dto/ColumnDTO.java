package com.orm.reflaction.handler.dto;

public class ColumnDTO {

	private String name;
	private String type;

	public ColumnDTO(String name, Class<?> type) {
		this.name = name;
		this.type = type.getName();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(Class<?> type) {
		this.type = type.getName();
	}

	@Override
	public String toString() {
		return "ColumnDTO [name=" + name + ", type=" + type + "]";
	}
}
