package com.orm.reflaction.handler;

import com.orm.reflaction.annotation.Column;
import com.orm.reflaction.annotation.Table;
import com.orm.reflaction.handler.dto.ColumnDTO;
import com.orm.reflaction.handler.dto.TableDTO;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;



/**
 * 
 * This class uses Java Reflection API to transform class fields and annotation
 * attributes to 'create table' sql query. There are a lot of hardcoded
 * properties and ugly manipulations with strings, I didn't want to use
 * third-party libraries, there is just a raw java.
 *
 */
public class EntityHandler {

	{
		try {
			Class.forName(JDBC_DRIVER);
		} catch (ClassNotFoundException e) {
			System.err.println(e.toString());
		}
	}

	private static final String JDBC_DRIVER = "org.postgresql.Driver";
	private static final String DB_URL = "jdbc:postgresql://localhost:5432/usersdb";
	private static final String USER = "postgres";
	private static final String PASSWORD = "programmer88";

	private final Map<String, String> types = new HashMap<String, String>() {
		{
			put("java.lang.Integer", "integer");
			put("java.lang.String", "VARCHAR(40)");
			put("int", "integer");
		}
	};

	public void createTable(Class<?> entity) {
		TableDTO table = buildTableDTO(entity);
		createTable(table);
	}

	private TableDTO buildTableDTO(Class<?> entityClass) {
		String tableName = "";
		if (entityClass.isAnnotationPresent(Table.class)) {
			Table annotation = entityClass.getAnnotation(Table.class);
			tableName = annotation.name();
		} else {
			tableName = entityClass.getSimpleName();
		}
		
		TableDTO table = new TableDTO(tableName);

		Field[] fields = entityClass.getDeclaredFields();
		List<ColumnDTO> columns = new LinkedList<>();

		for (Field field : fields) {
			if (field.isAnnotationPresent(Column.class)) {
				Column columnAnnotation = field.getAnnotation(Column.class);
				columns.add(new ColumnDTO(columnAnnotation.name(), field.getType()));
			} else {
				columns.add(new ColumnDTO(field.getName(), field.getType()));
			}
		}
		table.setColumns(columns);
		System.out.println("built DTO: " + table);
		return table;
	}

	private void createTable(TableDTO table) {
		try {
			Connection conn = DriverManager.getConnection(DB_URL, USER, PASSWORD);
			Statement stmt = conn.createStatement();
			String sql = getCreateTableQuery(table);
			stmt.executeUpdate(sql.toString());
		} catch (SQLException e) {
			System.err.println(e.toString());
		}
	}

	private String getCreateTableQuery(TableDTO table) {
		StringBuilder columns = new StringBuilder();
		for (ColumnDTO column : table.getColumns()) {
			if(column.getName().contains("id")){
				columns.append(column.getName()).append(" ").append(types.get(column.getType()))
						.append(" ").append("PRIMARY KEY").append(", ");
			}else{
				columns.append(column.getName()).append(" ").append(types.get(column.getType())).append(", ");
			}

		}

		columns.deleteCharAt(columns.length() - 2);

		StringBuilder sqlQuery = new StringBuilder("CREATE TABLE ").append(table.getName()).append("( ").append(columns)
				.append(");");
		System.out.println("generated sqlQuery: " + sqlQuery);
		return sqlQuery.toString();
	}
}
